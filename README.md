# Resultado Challenge

## proyecto frontend
https://gitlab.com/open18/so-frontend


## proyecto backend
https://gitlab.com/open18/so-backend


## Instalación con docker-compose 
```bash
docker-compose up -d
```

# Demo desplegado en AWS y Heroku

https://master.d2kj8htz4i4590.amplifyapp.com/


## License
Edwin Acosta

https://edwinacosta.co/

